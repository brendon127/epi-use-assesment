import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HierarchyComponent } from './hierarchy.component';
import { MatTreeModule, MatIconModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [HierarchyComponent],
  imports: [
    CommonModule,
    MatTreeModule,
    MatButtonModule,
    MatIconModule,
  ],
  exports: [
    HierarchyComponent
  ]
})
export class HierarchyModule { }
