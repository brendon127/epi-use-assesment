import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import { StaffInfoServiceService } from '../services/staff-info/staff-info-service.service';


@Component({
  selector: 'app-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss']
})
export class HierarchyComponent implements OnInit {

  nestedTreeControl: NestedTreeControl<any>;
  nestedDataSource: MatTreeNestedDataSource<any>;
  dataChange: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  constructor( private staffInfoService: StaffInfoServiceService ) {
    this.updateHierarchy();
  }
  private getChildren = (node: any) => { return observableOf(node.children); };
  
  hasNestedChild = (_: number, nodeData: any) => {return !(nodeData.type); };

  ngOnInit() {
  }
  updateHierarchy() {
    this.nestedTreeControl = new NestedTreeControl<any>(this.getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource<any>();
    const currentClass = this;
    this.staffInfoService.getHierarchy().subscribe(
      data => {
        currentClass.dataChange.subscribe ( dat => currentClass.nestedDataSource.data = dat);
        currentClass.dataChange.next(data);
      },
      error => console.log(error)
    );
  }

}
