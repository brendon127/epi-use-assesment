import { TestBed } from '@angular/core/testing';

import { StaffInfoServiceService } from './staff-info-service.service';

describe('StaffInfoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StaffInfoServiceService = TestBed.get(StaffInfoServiceService);
    expect(service).toBeTruthy();
  });
});
