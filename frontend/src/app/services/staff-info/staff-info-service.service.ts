import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffInfoServiceService {
  host: string;
  constructor(
    private http: HttpClient
  ) {
    // this.host = 'http://172.105.88.80:3000/';
    this.host = 'http://localhost:3000/';
  }
  getAllStaff() {
    const path = 'staff/all';
    const url = this.host + path;

    return this.http.get(url);
  }
  getHierarchy() {
    const path = 'staff/hierarchy';
    const url = this.host + path;
    return this.http.get(url);
  }
  getHighestEarners() {
    const path = 'staff/highest-earners';
    const url = this.host + path;
    return this.http.get(url);
  }
  updateEmployee(updatedEmployee) {
    const path = 'staff/update';
    const url = this.host + path;
    return this.http.post(url, updatedEmployee);
  }
}
