import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { ViewStaffModule } from './view-staff/view-staff.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTreeModule, MatIconModule, MatButtonModule } from '@angular/material';
import { HierarchyModule } from './hierarchy/hierarchy.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ViewStaffModule,
    MatTreeModule,
    MatButtonModule,
    MatIconModule,
    HierarchyModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
