import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HigestEarnersComponent } from './higest-earners.component';

describe('HigestEarnersComponent', () => {
  let component: HigestEarnersComponent;
  let fixture: ComponentFixture<HigestEarnersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HigestEarnersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HigestEarnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
