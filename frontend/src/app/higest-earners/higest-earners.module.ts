import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HigestEarnersComponent } from './higest-earners.component';

@NgModule({
  declarations: [ HigestEarnersComponent ],
  imports: [
    CommonModule
  ],
  exports: [
    HigestEarnersComponent
  ]
})
export class HigestEarnersModule { }
