import { Component, OnInit } from '@angular/core';
import { StaffInfoServiceService } from '../services/staff-info/staff-info-service.service';

@Component({
  selector: 'app-higest-earners',
  templateUrl: './higest-earners.component.html',
  styleUrls: ['./higest-earners.component.scss']
})
export class HigestEarnersComponent implements OnInit {
  staff: any;

  constructor( private staffInfoService: StaffInfoServiceService) {
    this.updateHighestEarners();
  }

  updateHighestEarners() {
    this.staff = [];
    const currentClass = this;
    this.staffInfoService.getHighestEarners().subscribe(
      data => {
        currentClass.staff = data;
      }
    );
  }

  ngOnInit() {
  }

}
