import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StaffInfoServiceService } from '../../services/staff-info/staff-info-service.service';
import { StaffFilterPipe } from '../pipes/staff-filter/staff-filter.pipe';
import { FilterState } from '../enums/filter-state.enum';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.scss'],
  // pipes: [ StaffFilterPipe ]
})
export class StaffListComponent implements OnInit {
  staffList: any;
  searchTerm: string;
  dateSearch: Date;
  nameButton: string;
  yearButton: string;
  sarie: string;
  filterState: FilterState;
  private toggle: boolean;
  @Output() messageEvent = new EventEmitter<any>();
  constructor(
    private staffInfoService: StaffInfoServiceService
  ) {
    this.filterState = FilterState.NAME;
  }

  sendMessage(staffMember) {
    let supervisor = this.staffList.filter( member => member._id === staffMember.reportsTo);
    supervisor = supervisor[0];
    const possibleSupervisors = this.staffList.filter(
      member =>  (staffMember.reportsTo !== member._id) && (member._id  !== staffMember._id) && (member.roleDesignation === 'Manager' || member.roleDesignation === 'Employee')
    );

    const staffMemberSupervisorPair = {
      staffMember,
      supervisor,
      possibleSupervisors
    };

    this.messageEvent.emit(staffMemberSupervisorPair);
  }

  ngOnInit() {
    const currentClass = this;
    this.staffInfoService.getAllStaff().subscribe(
      data => {
        this.staffList = data;
        currentClass.sendMessage(this.staffList[0]);
      },
      error => console.log(error),
    );
  }
  setName() {
    this.searchTerm = '';
    this.filterState = FilterState.NAME;
  }
  setYear() {
    this.searchTerm = '';
    this.filterState = FilterState.YEAR;
  }

  isName() {
    return this.filterState === FilterState.NAME ? true : false;
  }
  isYear() {
    return this.filterState === FilterState.YEAR ? true : false;
  }

}
