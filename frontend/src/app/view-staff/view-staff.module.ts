import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewStaffComponent } from './view-staff.component';
import { StaffListComponent } from './staff-list/staff-list.component';
import { StaffDetailsComponent } from './staff-details/staff-details.component';
import { StaffFilterPipe } from './pipes/staff-filter/staff-filter.pipe';
import { FormsModule } from '@angular/forms';
import { DateFilterPipe } from './pipes/staff-filter/date-filter.pipe';
import { HierarchyModule } from '../hierarchy/hierarchy.module';
import { HigestEarnersModule } from '../higest-earners/higest-earners.module';
// import { HierarchyComponent } from '../hierarchy/hierarchy.module';

import { MatTreeModule, MatIconModule, MatButtonModule } from '@angular/material';


@NgModule({
  declarations: [
    ViewStaffComponent,
    StaffListComponent,
    StaffDetailsComponent,
    StaffFilterPipe,
    DateFilterPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HierarchyModule,
    HigestEarnersModule
  ],
  exports: [ ViewStaffComponent ]

})
export class ViewStaffModule { }

