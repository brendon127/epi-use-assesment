import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-staff',
  templateUrl: './view-staff.component.html',
  styleUrls: ['./view-staff.component.scss']
})
export class ViewStaffComponent implements OnInit {
  message = 'hello';
  activeStaffMember: any;
  currentSupervisor: any;
  possibleSupervisors: any;
  isActive: boolean;

  constructor() {
    this.isActive = false;
  }


  ngOnInit() {
  }
  receiveMessage(staffMemberSupervisorPair) {
    this.activeStaffMember = staffMemberSupervisorPair.staffMember;
    this.possibleSupervisors = staffMemberSupervisorPair.possibleSupervisors;
    this.currentSupervisor = staffMemberSupervisorPair.supervisor;
    this.isActive = true;
  }

}
