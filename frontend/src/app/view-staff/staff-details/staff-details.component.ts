import { Component, OnInit, Input, EventEmitter, Output, ViewChild} from '@angular/core';
import { StaffInfoServiceService } from '../../services/staff-info/staff-info-service.service';

import { HigestEarnersComponent } from '../../higest-earners/higest-earners.component';
import { HierarchyComponent } from '../../hierarchy/hierarchy.component';
@Component({
  selector: 'app-staff-details',
  templateUrl: './staff-details.component.html',
  styleUrls: ['./staff-details.component.scss']
})
export class StaffDetailsComponent implements OnInit {
  @ViewChild(HigestEarnersComponent, {static: false} ) higestEarners;
  @ViewChild(HierarchyComponent, {static: false} ) hierarchy;
  @Input() currentMember: any;
  @Input() currentSupervisor: any;
  @Input() possibleSupervisors: any;
  private newSupervisor: any;
  constructor(
    private staffInfoService: StaffInfoServiceService
  ) { }
  getDOB() {
    const date = new Date(this.currentMember.dateOfBirth).toISOString().substring(0, 10);
    return date;
  }
  update() {
    const currentClass = this;
    this.currentMember.name =  (document.getElementById('name') as HTMLInputElement).value;
    this.currentMember.surname =  (document.getElementById('surname') as HTMLInputElement).value;
    this.currentMember.salary =  (document.getElementById('salary') as HTMLInputElement).value;
    this.currentMember.dateOfBirth =  (document.getElementById('dob') as HTMLInputElement).value;
    this.currentMember.roleDesignation = (document.getElementById('role') as HTMLInputElement).value;
    const supervisorEl = (document.getElementById('supervisor') as HTMLInputElement);

    if ( supervisorEl ) {
      this.currentMember.reportsTo = supervisorEl.value;
    } else {
      this.currentMember.reportsTo = null;
    }

    // this.currentMember.reportsTo = (document.getElementById('supervisor') as HTMLInputElement).value;
    this.staffInfoService.updateEmployee( this.currentMember).subscribe(
      data =>  {
        const response = data as any;
        if (( response.status ) === 'success') {
          currentClass.higestEarners.updateHighestEarners();
          currentClass.hierarchy.updateHierarchy();
          alert('successfully updated');
        } else {
          alert('Update failed\nCircular reporting detected ');
        }
      },
      error => console.log(error)
    );
  }

  hasSupervisor() {
    return this.currentMember.reportsTo !== null ? true : false;
  }

  ngOnInit() {
  }

}
