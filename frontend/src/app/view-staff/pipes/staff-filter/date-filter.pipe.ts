import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFilter'
})
export class DateFilterPipe implements PipeTransform {

  transform(staffMembers: any[], searchTerm: any): any {
    if (!staffMembers || !searchTerm ) {
      return staffMembers;
    }

    // return staffMembers.filter( (staffMember) => new Date(staffMember.dateOfBirth) > searchTerm);
    return staffMembers.filter( (staffMember) => {
      searchTerm = new Date(searchTerm);
      const staffDate = new Date(staffMember.dateOfBirth);
      return staffDate >= searchTerm;
    });

  }

}
