import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'staffFilter',
  pure: false
})
export class StaffFilterPipe implements PipeTransform {

  transform(staffMembers: any[], searchTerm: any): any[] {
    if ( !staffMembers || !searchTerm) {
      return staffMembers;
    }

    return staffMembers.filter( staffMember =>
      staffMember.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);

  }

}
