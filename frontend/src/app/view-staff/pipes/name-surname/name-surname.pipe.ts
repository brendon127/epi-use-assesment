import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameSurname'
})
export class NameSurnamePipe implements PipeTransform {

  transform(value: any): any {
    return value.name  + ' ' + value.surname;
  }

}
