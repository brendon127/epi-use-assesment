import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginDataService } from '../services/login-data/login-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ LoginDataService ]
})
export class LoginComponent implements OnInit {
  @ViewChild('username', { static: false}) usernameEl: ElementRef;
  statusLogin: any;
  form: FormGroup;
  username: string;
  password: string;


  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private loginDataService: LoginDataService,
    public router: Router
  ) {
    this.form = fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }
  submit(data) {
    if (this.loginDataService.login(data)) {

    }

  }

  ngOnInit() {
  }

}
