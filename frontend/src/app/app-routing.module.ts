import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ViewStaffComponent } from './view-staff/view-staff.component';
import { HierarchyComponent } from './hierarchy/hierarchy.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'viewStaff', component: ViewStaffComponent},
  { path: 'hierarchy', component: HierarchyComponent},
  { path: '', redirectTo: '/viewStaff', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
