import * as mongoose from 'mongoose';

export const StaffMemberSchema = new mongoose.Schema({
  name: String,
  surname: String,
  dateOfBirth: Date,
  employeeNumber: Number,
  roleDesignation: String,
  salary: Number,
  reportsTo: String,
});
