import { Module } from '@nestjs/common';
import { StaffMemberController } from './staff-member.controller';
import { StaffMemberService } from './staff-member.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StaffMemberSchema } from './schemas/staff-member.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'StaffMember', schema: StaffMemberSchema }]),
  ],
  providers: [StaffMemberService],
  controllers: [StaffMemberController],
})
export class StaffMemberModule {}
