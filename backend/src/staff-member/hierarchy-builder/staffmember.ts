
export class StaffMemberWithChildren {
  public id: string;
  public name: string;
  public surname: string;
  public dateOfBirth: Date;
  public employeeNumber: number;
  public roleDesignation: string;
  public salary: number;
  public reportsTo: number;
  public children: any;
  constructor(member) {
    this.id = member.id;
    this.name = member.name;
    this.surname = member.surname;
    this.dateOfBirth = member.dateOfBirth;
    this.employeeNumber = member.employeeNumber;
    this.roleDesignation = member.roleDesignation;
    this.salary = member.salary;
    this.reportsTo = member.reportsTo;
    this.children = [];
  }
  addChildren(arr) {
    this.children.push(arr);
  }
}
