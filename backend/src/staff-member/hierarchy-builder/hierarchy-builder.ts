import { StaffMemberWithChildren } from './staffmember';

export class HierarchyBuilder {
  private staffMembers: any;
  private hierarchy: any;
  constructor( staffMembers) {
    this.staffMembers = [];

    staffMembers.forEach( ( member ) => {
      this.staffMembers.push( new StaffMemberWithChildren(member));
    });

  }

  build() {
    const allHierarchies = this.buildRecursive(this.staffMembers[0]);
    const wanted = allHierarchies.filter( staffMember => staffMember.reportsTo ===  null); // Highest ranked employee
    return wanted;
  }

  buildRecursive(staffMember) {
    const childrenQueue = this.getChildren(staffMember);
    const originalQueue = Array.from(childrenQueue);
    while (childrenQueue && childrenQueue.length) {
      this.buildRecursive( childrenQueue.shift());
    }
    const currentClass = this;
    originalQueue.forEach( (item) => {
      currentClass.staffMembers[staffMember.employeeNumber].addChildren( (item) );
    });
    return this.staffMembers;
  }

  getChildren(currentMember) {
    const children = this.staffMembers.filter( staffMember => staffMember.reportsTo === currentMember.id);
    return children;
    if ( children && children.length) {
      return children;
    }
    else {
      return ['empty'];
    }
  }
}
