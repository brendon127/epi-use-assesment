export class CreateStaffMemberDTO {
  readonly name: string;
  readonly surname: string;
  readonly dateOfBirth: Date;
  readonly employeeNumber: number;
  readonly roleDesignation: string;
  readonly salary: number;
  readonly reportsTo: string;
}
