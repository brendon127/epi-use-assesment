import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { StaffMemberService } from './staff-member.service';
import { CreateStaffMemberDTO } from './dto/create-staff-member.dto';
import { HierarchyBuilder } from './hierarchy-builder/hierarchy-builder';

@Controller('staff')
export class StaffMemberController {
  constructor( private staffMemberService: StaffMemberService ) {}
  @Get('all')
  async getAllStaffMembers(@Res() res ) {
    const staffMembers = await this.staffMemberService.getAllStaffMembers();
    return res.status(HttpStatus.OK).json(staffMembers);
  }
  @Get('highest-earners')
  async getHigestEarners(@Res() res ) {
    const staffMembers = await this.staffMemberService.getHighestEarners();
    return res.status(HttpStatus.OK).json(staffMembers);
  }
  @Get('hierarchy')
  async getHierachy( @Res() res) {
    const staffMembers = await this.staffMemberService.getAllStaffMembers();
    const hierarchy = new HierarchyBuilder( staffMembers );
    return res.status(HttpStatus.OK).json(hierarchy.build());
  }

  @Post('update')
  async updateStaffMember( @Res() res, @Body() staffMember) {
    // check if circular reportsTo exists
    const isCircular = await this.staffMemberService.isCircular(staffMember);
    if (!isCircular) {
      const returnObj = {
        status: 'success',
      };
      const conformation = await this.staffMemberService.updateStaffMember( staffMember );
      return res.status(HttpStatus.OK).json( returnObj );
    }
    else {
      const returnObj = {
        status: 'failed',
      };
      return res.status(HttpStatus.OK).json( returnObj);
    }
  }
  @Post('create')
  async addStaffMember(@Res() res, @Body() createStaffMemberDTO: CreateStaffMemberDTO) {
    const staffMember =  await this.staffMemberService.addStaffMember(createStaffMemberDTO);
    return res.status(HttpStatus.OK).json( {
      message: 'sucess',
      staffMember,
    });
  }

}
