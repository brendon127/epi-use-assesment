import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { StaffMember } from './interfaces/staff-member.interface';
import { CreateStaffMemberDTO } from './dto/create-staff-member.dto';

@Injectable()
export class StaffMemberService {
  constructor(@InjectModel('StaffMember') private readonly staffMemberModel: Model<StaffMember>) {}

  async getAllStaffMembers(): Promise<StaffMember[]> {
    const staffMembers = await this.staffMemberModel.find().exec();
    return staffMembers;
  }
  async addStaffMember(staffMember): Promise<any> {
    const newone = await this.staffMemberModel(staffMember);
    return newone.save();
  }
  async isCircular( staffMember ) {
    const children = await this.staffMemberModel.find( {reportsTo: staffMember._id, _id: staffMember.reportsTo}).exec();
    if ( children.length !== 0) {
      return true; // it is circular
    }
    else {
      return false; // not circular
    }
  }

  async updateStaffMember( staffMember ): Promise<StaffMember> {
    const conformation = await this.staffMemberModel.updateOne( {_id: staffMember._id} ,
      {
        $set: {
          name: staffMember.name,
          surname: staffMember.surname,
          reportsTo: staffMember.reportsTo,
          roleDesignation: staffMember.roleDesignation,
          salary: staffMember.salary,
          dateOfBirth: staffMember.dateOfBirth,
        },
      },
    )
    .exec();
    return conformation;
  }
  async getHighestEarners(): Promise<StaffMember[]> {
    const highestEarners = [];
    const manager = await this.staffMemberModel.findOne( { roleDesignation: 'Manager' }).sort({salary: 'desc'}) .exec();
    const employee = await this.staffMemberModel.findOne( { roleDesignation: 'Employee' }).sort({salary: 'desc'}) .exec();
    const trainee = await this.staffMemberModel.findOne( { roleDesignation: 'Trainee' }).sort({salary: 'desc'}) .exec();
    highestEarners.push(manager);
    highestEarners.push(employee);
    highestEarners.push(trainee);
    return highestEarners;
  }
}
