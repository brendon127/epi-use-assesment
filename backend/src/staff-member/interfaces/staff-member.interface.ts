import { Document } from 'mongoose';

export interface StaffMember extends Document {
  readonly name: string;
  readonly surname: string;
  readonly dateOfBirth: Date;
  readonly employeeNumber: number;
  readonly salary: number;
  readonly reportsTo: number;
}
