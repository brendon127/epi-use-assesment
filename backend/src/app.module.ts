import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config-service';
import { StaffMemberModule } from './staff-member/staff-member.module';
import { StaffMemberController } from './staff-member/staff-member.controller';

import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('MONGODB_URI'),
      }),
      inject: [ConfigService],
    }),
    StaffMemberModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
