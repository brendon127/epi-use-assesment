# EPI-USE Assessment 

### Installation
 ```
 npm install 
 ```

### Copy environment variables

Copy the '.env.development.local' file attached in the submission email into the root folder of the backend server.
This file is not included in the repository because it contains the database credentials.

 ### Runnning
 ```
#development
npm run start:dev
#production
npm run start:prod
 ```

### Database

The staff member data is stored in a MongoDB database hosted on [https://www.mongodb.com/cloud/atlas](https://www.mongodb.com/cloud/atlas)
The schema is structured as follows:
```
{
	_id: <auto generated unique id>
	name: <string>,
	surname: <string>,
	dateOfBirth: <timestamp>,
	employeeNumber: <int>,
	salary: <double>,
	roleDesignation: <string>,
	reportsTo: <id of another employee or manager>
}
```

### Documentation
To view the documentation, open documentation/index.html in any web browser.
